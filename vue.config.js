module.exports = {
    devServer:{
        proxy:{
            "/api":{
                secure: false,
                target:"https://aip.baidubce.com",
                changeOrigin: true,
                pathRewrite:{
                    "^/api" : "/mock"
                }
            }
        },
    }
};